# Do not remove the header from this file or other files created from it
# LICENSE General Public License Version 3
# However please give credit to the creator of the original code 
# Made by Vlad C.
# Provided on AS IS basis without any warranty of any kind, either expressed or implied
# Use it at your own risk
# Minesweeper dependencies
from __future__ import print_function
import os


def cls():
	'''clear console window'''
	os.system( 'cls' if os.name=='nt' else 'clear')

def show(grid,mnumber):
	'''prints out grid as player knows it'''
	
	end=9 #last character of the line
	new=0
	numbers=['0','1','2','3','4','5','6','7','8','9']
	l=0
	j=0

	print('  ',end=' ')
	for i in numbers: #prints numbers as a top line
		print (i, end='')
	print('\n')#empty line
	for i in range(0,100,1):
		j+=1
		if end==i:
			print (grid[i]) #creates new line
			end+=10
		elif i==new:
			print (numbers[l], '', grid[i], end='') #letter at the beginning of the line
			if l!=9:
				l+=1
			new+=10
		else:
			print (grid[i], end='') #prevents from printing next time on a new line
	print ('\n','Mines left:',mnumber)

def selfncheck(grid,matrix,ident): #shenanigans are present
	'''check self and neighbours for being a mine. If not mined open neighbours'''
	identifier=[]
	identifier.append(ident)
	for ident in identifier:
		cellvalue=0

		if ident==0: #top left corner +1 +11 +10
			if matrix[ident+1]=='x':
				cellvalue+=1
			if matrix[ident+10]=='x':
				cellvalue+=1
			if matrix[ident+11]=='x':
				cellvalue+=1
			if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
				identifier.append(ident+1)
				identifier.append(ident+11)
				identifier.append(ident+10)

		if ident==9: #Top right corner -1 +10 +9
			if matrix[ident-1]=='x':
				cellvalue+=1
			if matrix[ident+10]=='x':
				cellvalue+=1
			if matrix[ident+9]=='x':
				cellvalue+=1
			if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
				identifier.append(ident-1)
				identifier.append(ident+10)
				identifier.append(ident+9)

		if ident==90: #bot left corner +1 -10 -9
			if matrix[ident+1]=='x':
				cellvalue+=1
			if matrix[ident-10]=='x':
				cellvalue+=1
			if matrix[ident-9]=='x':
				cellvalue+=1
			if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
				identifier.append(ident+1)
				identifier.append(ident-9)
				identifier.append(ident+-10)

		if ident==99: #bot right corenr -10 -11 -1
			if matrix[ident-1]=='x':
				cellvalue+=1
			if matrix[ident-10]=='x':
				cellvalue+=1
			if matrix[ident-11]=='x':
				cellvalue+=1
			if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
				identifier.append(ident-1)
				identifier.append(ident-10)
				identifier.append(ident-11)

		if ident>0 and ident<9: #checking top row -1 +1 +10 +9 +11 cells only
			if matrix[ident-1]=='x':
				cellvalue+=1
			if matrix[ident+1]=='x':
				cellvalue+=1
			if matrix[ident+10]=='x':
				cellvalue+=1
			if matrix[ident+9]=='x':
				cellvalue+=1
			if matrix[ident+11]=='x':
				cellvalue+=1
			if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
				identifier.append(ident+1)
				identifier.append(ident-1)
				identifier.append(ident+10)
				identifier.append(ident+9)
				identifier.append(ident+11)

		if ident>90 and ident<99: #checking bot row +1 -1 -10 -9 -11
			if matrix[ident-1]=='x':
				cellvalue+=1
			if matrix[ident+1]=='x':
				cellvalue+=1
			if matrix[ident-10]=='x':
				cellvalue+=1
			if matrix[ident-9]=='x':
				cellvalue+=1
			if matrix[ident-11]=='x':
				cellvalue+=1
			if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
				identifier.append(ident+1)
				identifier.append(ident-1)
				identifier.append(ident-10)
				identifier.append(ident-9)
				identifier.append(ident-11)

		for i in range(1,10,1):	
			if ident==i*10 and ident<90: #checking left side +1 -10 +10 -9 +11
				if matrix[ident-1]=='x':
					cellvalue+=1
				if matrix[ident+10]=='x':
					cellvalue+=1
				if matrix[ident-10]=='x':
					cellvalue+=1
				if matrix[ident-9]=='x':
					cellvalue+=1
				if matrix[ident+11]=='x':
					cellvalue+=1
				if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
					identifier.append(ident+1)
					identifier.append(ident-10)
					identifier.append(ident-9)
					identifier.append(ident+10)
					identifier.append(ident+11)	
	
			if ident==i*10+9 and ident<90: # checking right side -1 -10 +10 +9 -11
				if matrix[ident-1]=='x':
					cellvalue+=1
				if matrix[ident-10]=='x':
					cellvalue+=1
				if matrix[ident+10]=='x':
					cellvalue+=1
				if matrix[ident+9]=='x':
					cellvalue+=1
				if matrix[ident-11]=='x':
					cellvalue+=1
				if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
					identifier.append(ident-1)
					identifier.append(ident-10)
					identifier.append(ident+10)
					identifier.append(ident+9)
					identifier.append(ident-11)

			if ident>i*10 and ident<i*10+9 and ident<90: #checking +1 -1 -9 -11 -10 +9 +10 +11
				if matrix[ident+1]=='x':
					cellvalue+=1
				if matrix[ident-1]=='x':
					cellvalue+=1
				if matrix[ident-9]=='x':
					cellvalue+=1
				if matrix[ident-11]=='x':
					cellvalue+=1
				if matrix[ident-10]=='x':
					cellvalue+=1
				if matrix[ident+9]=='x':
					cellvalue+=1
				if matrix[ident+10]=='x':
					cellvalue+=1
				if matrix[ident+11]=='x':
					cellvalue+=1
				if cellvalue==0 and grid[ident]=='o': #if neighbours are not mines then check them too and count them as explored
					identifier.append(ident+1)
					identifier.append(ident-1)
					identifier.append(ident-9)
					identifier.append(ident-11)
					identifier.append(ident-10)
					identifier.append(ident+9)
					identifier.append(ident+10)
					identifier.append(ident+11)
		grid[ident]=str(cellvalue)
	return grid