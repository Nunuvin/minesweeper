# Do not remove the header from this file or other files created from it
# LICENSE General Public License Version 3
# However please give credit to the creator of the original code 
# Made by Vlad C.
# Provided on AS IS basis without any warranty of any kind, either expressed or implied
# Use it at your own risk
# Minesweeper 0.7

from __future__ import print_function
import random
import os
import minesweeper_dependencies


while True:
	grid=[''] #data matrix
	'''
	o-empty
	x-mine
	'''
	usr_grid=['']#shows known grid
	'''
	0-8 number of mines around
	x-mine
	!-suspected mine
	o-unexplored
	'''
	j=''
	mine_number=raw_input('How max mines would you like to use?, type 101 to quit ')
	mine_number=int(mine_number)
	if mine_number==101:
		break
	mine_counter=mine_number
	mine_numero=mine_number
	game=False
	for i in range(100): #creating grid list
		if i==0:
			grid[i]='o'
			usr_grid[i]='o'
		else:
			grid.append('o')
			chance=random.randrange(2)
			if chance==1 and mine_number!=0: #if gen says to append a mine then do that as long as there are left
				grid[i]='x'
				mine_number-=1
			usr_grid.append('o')

	while game==False:

		cls=True
		checking=True
		minesweeper_dependencies.show(usr_grid, mine_counter) #draws grid known to player
		usr_input=raw_input('Which cell to check? ')
		usr_input=int(usr_input)
		usr_action=raw_input('what do you want to do: potential mine type !, remove ! type o, explore cell type e, see mines enter m: ')
		if usr_action=='m':
			cls=False
			minesweeper_dependencies.show(grid, mine_counter)
		if usr_action=='e':
			if grid[usr_input]=='o': #empty
				usr_grid=minesweeper_dependencies.selfncheck(usr_grid,grid,usr_input)
			elif grid[usr_input]=='x': #mined
				usr_grid[usr_input]='x'
				usr_input= raw_input ('You have exploded. Returning to the main menu')
				break
		elif usr_action=='!': #usr suspects a mine
			usr_grid[usr_input]='!'
			mine_counter-=1 #decrease number of potential mines
		elif usr_action=='o':
			if usr_grid[usr_input]=='!':
				usr_grid[usr_input]='o'
				mine_counter+=1
		if mine_counter==0: #check if user identified everythin correctly
			acc=0
			for i in range (100):
				if (grid[i]=='x' and usr_grid[i]=='!'):
					acc+=1
			if acc==mine_numero:
				cls=False
				j=raw_input ('You won! Press enter to return to the main menu')
				break
		if cls==True:
			minesweeper_dependencies.cls() #clears screen after turn
		